ls #!/bin/bash
shopt -s expand_aliases
export RUCIO_ACCOUNT=mmittal
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
lsetup fax
export STORAGEPREFIX=root://atldtn1.slac.stanford.edu//
export X509_USER_PROXY=/afs/cern.ch/user/m/mmittal/x509up_u6814
#export X509_USER_PROXY=x509up_u20074
CURRDIR=/afs/cern.ch/work/m/mmittal/private/08082017/n0010/muTrigNt_read
cd $CURRDIR
# run the code
dirname=08032018
mkdir $dirname

mkdir $dirname/RUNNUMBER
run_example_looper -i INPUTFILE -o OUTPUTFILE -c STREAM
mv OUTPUTFILE $dirname/RUNNUMBER

