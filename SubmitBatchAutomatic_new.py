# To submit job automatically over the whole sample at 10 files at one time
# The input is based upon runnumber and prefix
#Submit batch jobs

def MakeBatchScript(textfilename, outputfilename, runNum):
    outnumfile = textfilename.replace('fileList_','').replace('.txt','.sh')
    shellfilename = 'batchsubmit_'+outnumfile
    fout = open(shellfilename, 'w')
    for iline in open('testbatch.sh'):
        iline = iline.replace('INPUTFILE', textfilename)
        iline = iline.replace('OUTPUTFILE', outputfilename)
        iline = iline.replace('STREAM', "zb")
        iline = iline.replace('RUNNUMBER', runNum)
        fout.write(iline)
    fout.close()
    return shellfilename

'''
for pre, runNum in zip(prefix_,runnum):# for making a list
prefix_ = ['f746','f750','f756','f749','f758']

#runnum = ['n010c.00309314','n010c.00310216','n010c.00310574','n010c.00309375','n010c.00311287'] #zb
#runnum = ['n010c.00309314','n010c.00310216','n010c.00310574','n010d.00309375','n010e.00311287'] #express

    filestring = '/eos/user/m/mmittal/Qualification_task/user.asoffa.muTrigNt.'+runNum+'.physics_ZeroBias.recon.ESD.'+pre+'_EXT0/'
    filestring = '/eos/user/m/mmittal/Qualification_task/user.asoffa.muTrigNt.'+runNum+'.express_express.recon.ESD.'+pre+'_EXT0/'


for runNum in ['MB_without_cavernBkg.mu40']:
    filestring = '/eos/user/m/mmittal/Qualification_task/user.asoffa.muTrigNt.n010b.'+runNum+'.run2.ESD.1a.v1_EXT0_EXT0/'

for runNum in ['MB_with_cavernBkg.mu40']:
    filestring = '/eos/user/m/mmittal/Qualification_task/user.asoffa.muTrigNt.n010b.'+runNum+'.run2.ESD.1a.v7_EXT0_EXT0/'

for runNum in ['Zmumu_with_cavernBkg.mu40']:
    filestring = '/eos/user/m/mmittal/Qualification_task/user.asoffa.muTrigNt.n010b.'+runNum+'.run2.ESD.v1b.t2_EXT0_EXT0/'
'''

for runNum in [ 'MC_147807.r9899']:
    filestring = '/eos/atlas/atlascerngroupdisk/perf-muon/user.asoffa.muTrigNt.n010.147807.PowhegPythia8_AU2CT10_Zmumu.recon.ESD.e1564_s2988_s3000_r9899_EXT0/'

    textfilename_total = 'fileList_'+runNum+'.txt'
    os.system('ls -1 ' +filestring+' >& '+textfilename_total)
    newfile = textfilename_total.replace("fileList","AllFiles")
    fnew = open(newfile,"w")
    for iline in open(textfilename_total):
       # newline = 'root://eosuser//' +filestring + iline  # for accessing from group eos
        newline = filestring + iline
        print newline
        fnew.write(newline)
    fnew.close()
    
    List = open(newfile).read().splitlines() #convert text file to List
    
    if (len(List) >= 10 ) :
        loopnum = (len(List)/10 )+2
    if (len(List) < 10 ) :
        loopnum = 2
    
    loopnum = 400
    print loopnum
    i = 0
    for filenum in range(1,loopnum) :
        fileList = List[i:i+10]
        textfilename = 'fileList_'+runNum+'_'+str(filenum)+'.txt'
        outputfilename = 'HitRate_RunNum_'+runNum+'_'+str(filenum)+'.root'
        with open(textfilename, "w") as output:
            for filenum in fileList:
                output.write(filenum) 
                output.write('\n') 
        shellName = MakeBatchScript(textfilename, outputfilename, runNum)
        print shellName
        os.system('chmod +x '+shellName)     
        os.system('bsub -q 1nd ' +shellName)
        i = i+10

