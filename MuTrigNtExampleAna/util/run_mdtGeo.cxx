
#include <iostream>
#include <string>
#include "TChain.h"
#include "MuTrigNtExampleAna/MdtGeoDumper.h"
#include "PhaseIIMuonTriggerNtuple/ChainHelper.h"
#include "PhaseIIMuonTriggerNtuple/string_utils.h"


void printHelp() {
    std::cout << "  Options:\n"
              << "  -d debug printout level\n"
              << "     defaults: 0 (quiet)\n"

              << "  -i input (file, list, or dir)\n"
              << "     defaults: ''\n"

              << "  -n MDT chamber name \n"
              << "     defaults: empty. Loops over all\n"

              << "  -h print this help" << std::endl;
}

int main(int argc, char **argv) {
    int dbg = 0;
    std::string name = "";
    std::string input = "";
    std::cout << "run_mdtGeo\n\n";

    /** Read inputs to program */
    for (int i = 1; i < argc; ++i) {
        if      (strcmp(argv[i], "-n") == 0) name = argv[++i];
        else if (strcmp(argv[i], "-d") == 0) dbg = atoi(argv[++i]);
        else if (strcmp(argv[i], "-i") == 0) input = argv[++i];
        else {
            printHelp();
            return 0;
        }
    }

    if (input.empty()) {
        std::cout << "You must specify an input\n";
        return 1;
    }

    if (dbg) {
        std::cout << "Being called as: " << TrigNtup::utils::commandLineArguments(argc, argv) << "\n";
    }

    std::cout << "flags:\n"
              << "  dbg     " << dbg    << "\n"
              << "  input   " << input  << "\n"
              << "  name    " << name << "\n"
              << std::endl;

    bool verbose = dbg > 0;
    // Build the input chain
    TChain chain {"MDTGeoNt"};
    ChainHelper::addInput(&chain, input, verbose);
    Long64_t nEntries = chain.GetEntries();
    chain.ls();

    // Build the TSelector
    MdtGeoDumper dumper{&chain};
    dumper.setDebug(dbg);
    dumper.setMdtChamber(name);
    chain.Process(&dumper);


    return 0;
}

