
#pragma once

// Root Packages
#include "TTree.h"
#include "TChain.h"

#include "PhaseIIMuonTriggerNtuple/MuonTriggerNtAna.h"

#include <fstream>
#include <string>

class MdtGeoDumper : public MuonTriggerNtAna {
public:
    MdtGeoDumper(TChain *c);
    virtual ~MdtGeoDumper(){};

    // Begin is called before looping on entries
    virtual void Begin(TTree *tree);

    // Terminate is called after looping is finished
    virtual void Terminate();

    // Main event loop function
    virtual Bool_t  Process(Long64_t entry);

    // Dump cutflow - if derived class uses different cut ordering,
    // override this method
    virtual void dumpEventCounters();

    // debug check
    bool debugEvent();

    // select chamber 
    void setMdtChamber(std::string name) {m_mdtChamber = name;};


    
    ClassDef(MdtGeoDumper, 1);

protected:
    TChain *m_input_chain;  // input chain being processed
    uint    n_readin;       // Event counters
   
    std::string m_data_dir;
    
    std::string m_mdtChamber;
};

