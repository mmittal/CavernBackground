/*
root
.x MuTrigNtExampleAna/script/checkMuTrigNt.C

To browse an muTrigNt ntuple
browse("/data/home/ataffard/workarea/PhaseII_MDT/InputFiles/muTrigNt/n008/n008d/user.asoffa.muTrigNt.n008d_try4.147807.PowhegPythia8_AU2CT10_Zmumu.recon.ESD.e1564_s2988_s3000_r8974_EXT0.txt");

To browse an MDTGeo Ntuple
browse("/home/ataffard/workarea/PhaseII_MDT/InputFiles/mdtGeoNt/mc15_14TeV.147807_Zmumu_mu0_MDTGeo_JO-auto.root","MDTGeoNt");


 */

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>

#include "TChainElement.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TRandom.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TRef.h"

#include "PhaseIIMuonTriggerNtuple/ChainHelper.h"

TChain* nt;
TFile* f;



void browse(const std::string name, std::string ntName="MuonTriggerNt")
{
    std::cout << "Loading file from " << name << std::endl;
    nt = new TChain(ntName.c_str());
    ChainHelper::addInput(nt, name,true);
    nt->ls();
    f = nt->GetFile(); //grab current filea
}


void browseOld(const char* name)
{
  nt = new TChain("MuonTriggerNt");
  nt->Add(name);
  f = nt->GetFile(); //grab current file
}


